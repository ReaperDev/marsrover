// Places and runs a series of movement on a rover
function runRover(roverData) {
    placeRover(roverData['position'][0], roverData['position'][1], roverData['position'][2], $('#cg' + curRover));
    moveList = roverData['moves'].split("");
    numOfMoves = moveList.length;
    curRoverMoves = numOfMoves;

    rover = $('#cg' + curRover);
    $(rover).prop('title', 'Ending position: ' + roverData['endingPosition']);
    roverRotation = $(rover).data('rotation');
    $.each(moveList, function(index, value) {
        if (value == 'M') {
            cur = $(rover).position();
            moveRover($(rover), 1, roverRotation);
        } else if (value == 'R' || value == 'L') {
            if (roverRotation == 0 && value == 'L')
                roverRotation -= 90;
            else if (value == 'R')
                roverRotation += 90;
            else
                roverRotation -= 90;
            rotateRover(rover, roverRotation);
        }
    })
    curRover++;
}

// Run the rotation animation on rover
function rotateRover(rover, rotation) {
    $(rover).data('rotation', rotation);
    $(rover).animate({  rotation: rotation }, {
        step: function() {
          $(this).css('-webkit-transform','rotate('+this.rotation+'deg)');
          $(this).css('-moz-transform','rotate('+this.rotation+'deg)');
          $(this).css('transform','rotate('+this.rotation+'deg)');
        },
        duration: 1000,
        complete:function () {
            curRoverMove++;
            checkIfComplete();
        }
    },'linear');
}

// Moves the rover
function moveRover(rover, moves, rotation) {
    cur = $(rover).position();
    if (rotation > 360) {
        rotation -= 360;
    } else if (rotation < 0) {
        rotation +=360;
    }
    if (rotation == 0) {
        distance = moves * sectionH;
        $(rover).animate(
            {top: "-=" + distance + "px"},
            {duration: 1000,
                complete:function () {
                    curRoverMove++;
                    checkIfComplete();
                }
            });
    } else if (rotation == 90) {
        distance = moves * sectionW;
        $(rover).animate(
            {left: "+=" + distance + "px"},
            {duration: 1000,
                complete:function () {
                    curRoverMove++;
                    checkIfComplete();
                }
            });
    } else if (rotation == 180) {
        distance = moves * sectionH;
        $(rover).animate(
            {top: "+=" + distance + "px"},
            {duration: 1000,
                complete:function () {
                    curRoverMove++;
                    checkIfComplete();
                }
            });
    } else if (rotation == 270) {
        distance = moves * sectionW;
        $(rover).animate(
            {left: "-=" + distance + "px"},
            {duration: 1000,
                complete:function () {
                    curRoverMove++;
                    checkIfComplete();
                }
            });
    }
    return;
}

// Checks if the all the movemens have been completed for a given rover
function checkIfComplete() {
    if (curRoverMove == curRoverMoves) {
        if (rovers.length >= curRover) {
            runRover(rovers[curRover-1]);
            curRoverMove = -1;
        }
    }
}

// Places the rover in it's starting position and orientation
function placeRover(x, y, d, rover) {
    $(rover).appendTo('.main-container');
    $(rover).css('height', sectionH + 'px').css('width', sectionW + 'px');
    var posX = (parseInt(x) * sectionW) + 'px';
    var posY = (availableH - ($(rover).outerHeight()*(parseInt(y)+1))) + 'px';
    $(rover).css('left', posX);
    $(rover).css('top', posY);
    $(rover).data('rotation', 0);
    if (d == 'E') {
        rotateRover(rover, 90);
        $(rover).data('rotation', 90);
    } else if (d == 'S') {
        rotateRover(rover, 180);
        $(rover).data('rotation', 180);
    } else if (d == 'W') {
        rotateRover(rover, 270);
        $(rover).data('rotation', 270);
    }
    $(rover).css('display', 'block');
}

// Resets the rover to 0 degree orientation and outside of the grid
function resetRovers() {
    $.each(rovers, function(index, value) {
        $('#cg' + (index+1)).appendTo('#roverContainer').css('display', 'none');
        $('#cg' + (index+1)).data('rotation', 0);
        $('#cg' + (index+1)).css('-webkit-transform','rotate('+0+'deg)');
        $('#cg' + (index+1)).css('-moz-transform','rotate('+0+'deg)');
        $('#cg' + (index+1)).css('transform','rotate('+0+'deg)');
    });
}

// Processese the json data returned from the file input upload
function processOutput(data) {
    data = JSON.parse(data);
    if ("success" in data) {
        $('.alert-text').html(data['message']);
        $('.alert').css('display', 'block');
        $('#inputModal').modal('hide');
        return;
    }
    $('#welcomeText').html('Output has been retrieved.<br/><button type="submit" class="btn" onclick="runSimulation();">Run Simulation</button>')
    $('#outputContainer').html(data['output']);
    $('#outputBtn').prop('disabled', false);
    gridH = parseInt(data['height']) + 1;
    gridW = parseInt(data['width']) + 1;

    pageH = Math.round($('.page').height());
    pageW = Math.round($('.page').width());
    $('.main-container').css('height', parseInt(pageH - 50) + 'px');
    $('.main-container').css('width', parseInt(pageH - 50) + 'px');
    availableH = $('.main-container').outerHeight();
    availableW = $('.main-container').outerWidth();
    sectionH = availableH/gridH;
    sectionW =  availableW/gridW;

    $.each(data['rovers'], function(index, value) {
        rovers.push(value);
    });
    $('#inputModal').modal('hide');
    $('#outputModal').modal('show');
}

// begins the simulation of the rover's movement
function runSimulation() {
    resetRovers();
    $('#outputModal').modal('hide');
    $('#welcomeText').css('display', 'none');
    $('.main-container').css('display', 'block');
    curRover = 1;
    curRoverMoves = 0;
    curRoverMove = 0;
    runRover(rovers[curRover-1]);
}
