<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>CG Mars Rover Simulator</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
        <script src="{{ asset('js/rover.js')}}"></script>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js" integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E=" crossorigin="anonymous"></script>
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .logo {
                width: 30px;
            }

            .section {
                border: 1px solid red;
                min-height: 150px;
            }

            .position {
                color: #ffffff;
            }

            .rover {
                display: none;
                position: absolute;
            }

            .main-container {
                position: relative;
                width: 300px;
                height: 300px;
                /*background-color: #1d1d1d;*/
                background-image: url('img/mars2.jpg');
                background-repeat: no-repeat;
                background-size: cover;
                border: 1px solid #fff;
                border-radius: 10px;
            }

            .bottom-nav {
                text-align: center;
                padding-bottom: 10px;
                border-radius: 10px;
            }

            .btn {
                background-color: #B72825;
                border: 1px solid #ffffff;
                border-radius: 15px;
                font-weight: bold;
                color: #ffffff;
            }

            .alert {
                position: absolute;
                top: 0px;
                z-index: 1000;
                display: none;
            }
        </style>

        <script>
            var gridH = 5;
            var gridW = 5;
            var sctionH = 0;
            var sectionW = 0;
            var availableH = 0;
            var availableW = 0;
            var rovers = [];
            var curRover = 1;
            var curRoverMoves = 0;
            var curRoverMove = 0;

            $(document).ready( function() {
                $('.full-height').css('height', '85vh');

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $('#process-file').submit(function(e) {
                    e.preventDefault();

                    var formData = new FormData(this);
                    rovers = [];

                    $.ajax({
                        type:'POST',
                        url: "{{ url('/api/rover/process')}}",
                        data: formData,
                        cache:false,
                        contentType: false,
                        processData: false,
                        success: (data) => {
                            this.reset();
                            $('.alert').css('display', 'none');
                            processOutput(data);
                        },
                        error: function(data){
                            $('.alert-text').html('Please check input file and try again.');
                            $('.alert').css('display', 'block');
                            $('#inputModal').modal('hide');
                        }
                    });
                });
            });
        </script>
    </head>
    <body>
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="img/cg_logo.png" class="logo">
                    {{ config('app.name', 'cgRovers') }}
                </a>
                <div class="navbar-nav">
                    <a href="#" class="nav-item nav-link" data-toggle="modal" data-target="#aboutModal">About Project</a>
                    <a href="https://bitbucket.org/ReaperDev/marsrover/commits/" class="nav-item nav-link" target="_blank">Git Repo</a>
                </div>
            </div>
        </nav>

        <div class="page flex-center position-ref full-height" >
            <div class="alert alert-danger alert-dismissible fade show " style="">
                <div class="alert-text"></div>
                <button type="button" class="close" onclick="$('.alert').css('display', 'none')">&times;</button>
            </div>
            <div id="welcomeText">Upload an input file to begin.</div>
            <div class="main-container" style="display: none;">
            </div>

            <div class="bottom-nav fixed-bottom">
                <div class="btn-group" role="group">
                    <button id="uploadBtn" type="button" class="btn btn-secondary" data-toggle="modal" data-target="#inputModal">Upload Input</button>
                    <button id="helpBtn" type="button" class="btn btn-secondary" data-toggle="modal" data-target="#helpModal">Help</button>
                    <button id="outputBtn" type="button" class="btn btn-secondary" data-toggle="modal" data-target="#outputModal" disabled>View Output</button>
                </div>
            </div>

            <!-- Modals -->
            <div class="modal fade" id="aboutModal" tabindex="-1" role="dialog" aria-labelledby="aboutModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="aboutModalLabel">About Project</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            A squad of robotic rovers are to be landed by NASA on a plateau on Mars. This plateau, which is curiously rectangular, must be navigated by the rovers so that their on-board cameras can get a complete view of the surrounding terrain to send back to Earth.<br/><br/>
                            A rover's position and location is represented by a combination of x and y coordinates and a letter representing one of the four cardinal compass points. The plateau is divided up into a grid to simplify navigation. An example position might be 0, 0, N, which means the rover is in the bottom left corner and facing North.<br/><br/>
                            In order to control a rover, NASA sends a simple string of letters. The possible letters are 'L', 'R' and 'M'. 'L' and 'R' makes the rover spin 90 degrees left or right respectively, without moving from its current spot. 'M' means move forward one grid point, and maintain the same heading.
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="outputModal" tabindex="-1" role="dialog" aria-labelledby="outputModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="outputModalLabel">Output</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <pre id="outputContainer"></pre>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn" onclick="runSimulation();">Run Simulation</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="inputModal" tabindex="-1" role="dialog" aria-labelledby="inputModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="outputModalLabel">Upload Input File</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form id="process-file" action="javascript:void(0)" method="post" enctype="multipart/form-data">
                        <div class="modal-body">
                        @csrf
                            <div class="form-group">
                                <input type="file" class="form-control-file" name="file" id="file" aria-describedby="fileHelp">
                                <small id="fileHelp" class="form-text text-muted">Please choose input file</small>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn">Upload</button>
                        </div>
                    </form>
                </div>
              </div>
            </div>

            <div class="modal fade" id="helpModal" tabindex="-1" role="dialog" aria-labelledby="helpModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="helpModalLabel">Help</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        To simulate the rover's route, you will need to upload an input file which will return the final coordinates of the rover as well as it's orientation. A simulation can then be ran, to visulaize each rover's route.<br/><br/>
                        <h2>Input File</h2>
                        The first line of input is the upper-right coordinates of the plateau, the lower-left coordinates are assumed to be (0, 0).
                        <br/><br/>
                        The rest of the input is information pertaining to the rovers that have been deployed. Each rover has two lines of input. The first line gives the rover's position, and the second line is a series of instructions telling the rover how to explore the plateau.
                        <br/><br/>
                        The position is made up of two integers and a letter separated by spaces, corresponding to the x and y coordinates and the rover's orientation.<br/>
                        * Limit of four rovers
                        <br/><br/>
                        <h3>Example Input</h3>
                        5 5<br/>
                        1 2 N<br/>
                        LMLMLMLMM<br/>
                        3 3 E<br/>
                        MMRMMRMRRM<br/><br/>
                        <h2>Errors</h2>
                        If a rover's movement would cause a collision or cause it fall of of the plateau, an error will be returned and no output will be avaiable.
                    </div>
                </div>
              </div>
            </div>
        </div>
        <div id="roverContainer">
            <img id="cg1" class="rover" src="img/rover.png">
            <img id="cg2" class="rover" src="img/rover.png">
            <img id="cg3" class="rover" src="img/rover.png">
            <img id="cg4" class="rover" src="img/rover.png">
        </div>
    </body>
</html>
